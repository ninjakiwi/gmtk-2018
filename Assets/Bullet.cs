﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    Rigidbody2D rb;

    GameObject visualRenderer;

    [SerializeField] float startingVelocity = 100;

    [SerializeField] float lifetime = 5;

    [SerializeField] GameObject sparks;

    [SerializeField] float damage;

    [SerializeField] bool evil = false;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        visualRenderer = transform.GetChild(0).gameObject;
        rb.velocity = transform.up * startingVelocity;
    }

    // Update is called once per frame
    void Update()
    {
        lifetime -= Time.deltaTime;
        if (lifetime <= -2 || !visualRenderer)
        {
            Instantiate(sparks, transform.position, Quaternion.identity);
            Destroy(gameObject);
            return;
        }

        visualRenderer.transform.rotation = Quaternion.LookRotation(rb.velocity.normalized, Vector3.down) * Quaternion.Euler(0f, -90f, 90f);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (!other || !other.gameObject || !other.gameObject.activeInHierarchy)
        {
            return;
        }

        if ((!evil && other.CompareTag("Enemy")) || (evil && other.CompareTag("Player")))
        {
            var hm = other.GetComponent<HealthManager>();

            if (!hm)
            {
                hm = other.GetComponentInParent<HealthManager>();
            }

            if (hm)
            {
                hm.Damage(damage);
                Instantiate(sparks, transform.position, Quaternion.identity);
                GetComponent<CircleCollider2D>().enabled = false;
                transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (lifetime <= 0)
        {
            Instantiate(sparks, transform.position, Quaternion.identity);
            Destroy(gameObject);
            return;
        }
        GetComponent<ParticleSystem>().Play();
    }
}