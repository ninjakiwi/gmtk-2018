﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Serialization;
using UnityEditor;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public enum UpgradeType
    {
        FireRate,
        Health,
        Damage
    }

    [SerializeField] public int fireRateLvl = 0;
    [SerializeField] public string fireRateCurve;

    [SerializeField] public int healthLvl = 0;
    [SerializeField] public string healthCurve;

    [SerializeField] public int damageLvl = 0;
    [SerializeField] public string dmgCurve;

//    // Use this for initialization
//    void Start()
//    {
//    }
//
//    // Update is called once per frame
//    void Update()
//    {
//    }

    public void Upgrade(UpgradeType upgradeType)
    {
        switch (upgradeType)
        {
            case UpgradeType.Damage:
                damageLvl++;
                GetComponent<WeaponTriggerManager>().damage = ExpressionEvaluator.Evaluate<float>(dmgCurve.Replace("x", damageLvl.ToString()));
                ChangeColor();
                break;
            case UpgradeType.Health:
                healthLvl++;
                GetComponent<HealthManager>().UpgradeMaxHealth(ExpressionEvaluator.Evaluate<float>(dmgCurve.Replace("x", healthLvl.ToString())));
                ChangeColor();
                break;
            case UpgradeType.FireRate:
                fireRateLvl++;
                GetComponent<WeaponTriggerManager>().fireRate = ExpressionEvaluator.Evaluate<float>(dmgCurve.Replace("x", fireRateLvl.ToString()));
                ChangeColor();
                break;
        }
    }

    public void ChangeColor()
    {
        GetComponentInChildren<SpriteRenderer>().color = Color.HSVToRGB(damageLvl * 0.2f - healthLvl * 0.2f + fireRateLvl * 0.2f, Mathf.Min(1, (damageLvl + fireRateLvl + healthLvl) * 0.1f),1);
    }
}