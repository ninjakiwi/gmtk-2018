﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyManager : MonoBehaviour
{
	[SerializeField] Text moneyTxtVal;
	public static float money = 100;

//	// Use this for initialization
//	void Start () {
//
//	}
//
//	// Update is called once per frame
	void Update ()
	{
		moneyTxtVal.text = money.ToString();
	}

	public static bool Spend(float amount)
	{
		if (amount > money)
		{
			return false;
		}

		money -= amount;
		return true;
	}
}
