﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemyManger : MonoBehaviour {

	[SerializeField] float enemyDelay;
	[SerializeField] private GameObject pathsobj;
	private float realDelay;

	[SerializeField] private GameObject enemyPrefab;
	
	// Use this for initialization
	void Start ()
	{
		realDelay = enemyDelay;
	}
	
	// Update is called once per frame
	void Update ()
	{
		enemyDelay = Mathf.Max(0.1f,enemyDelay - Time.deltaTime * 0.01f);
		realDelay -= Time.deltaTime;
		if (realDelay <= 0)
		{
			realDelay = enemyDelay;
			// Make a enemy
			
			Instantiate(enemyPrefab,pathsobj.transform);
		}
	}
}
