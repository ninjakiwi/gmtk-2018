﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class BuildingManager : MonoBehaviour
{
    [SerializeField] GameObject buildingPanel;
    [SerializeField] GameObject upgradePanel;

    [SerializeField] GameObject artilleryTowerPlaceholder;
    [SerializeField] GameObject machineGunTowerPlaceholder;
    [SerializeField] GameObject shotgunTowerPlaceholder;

    [SerializeField] GameObject energyRegenBtn;
    [SerializeField] GameObject energyCapBtn;

    public static bool paused = false;

    public static GameObject selectedItem;

    EnergyManager em;

    // Use this for initialization
    void Start()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        em = manager.GetComponent<EnergyManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            paused = !paused;
            Time.timeScale = paused ? 0 : 1;
            buildingPanel.SetActive(paused);
            energyRegenBtn.SetActive(paused);
            energyCapBtn.SetActive(paused);
            if (!paused)
            {
                upgradePanel.SetActive(false);
                if (selectedItem)
                {
                    selectedItem.GetComponent<LevelManager>().ChangeColor();
                    selectedItem = null;
                }
            }
        }

        if (!paused)
        {
            return;
        }

        if (Input.GetButtonDown("Fire1") && GameObject.FindGameObjectsWithTag("Placeholder").Length == 0)
        {
            Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            RaycastHit2D ray = Physics2D.Raycast(mousePos, Vector2.up, 0.01f);

            if (ray.collider && ray.collider.CompareTag("Player"))
            {
                if (selectedItem)
                {
                    selectedItem.GetComponent<LevelManager>().ChangeColor();
                }
                selectedItem = ray.collider.transform.parent.gameObject;
                selectedItem.GetComponentInChildren<SpriteRenderer>().color = Color.grey;
                upgradePanel.SetActive(true);
            }
//            else if (selectedItem)
//            {
//                selectedItem.GetComponent<LevelManager>().ChangeColor();
//                selectedItem = null;
//            }
        }
        else if (Input.GetButtonDown("Fire2") && GameObject.FindGameObjectsWithTag("Placeholder").Length == 0)
        {
            if (selectedItem)
            {
                selectedItem.GetComponent<LevelManager>().ChangeColor();
                selectedItem = null;
                upgradePanel.SetActive(false);
            }
        }
    }

    public void UpgradeTower(string type)
    {
        switch (type.ToLower())
        {
            case "damage":
                selectedItem.GetComponent<LevelManager>().Upgrade(LevelManager.UpgradeType.Damage);
                break;
            case "health":
                selectedItem.GetComponent<LevelManager>().Upgrade(LevelManager.UpgradeType.Health);
                break;
            case "firerate":
                selectedItem.GetComponent<LevelManager>().Upgrade(LevelManager.UpgradeType.FireRate);
                break;
        }
    }

    [SerializeField] float energyCapUpAmount = 0.2f;
    [SerializeField] float energyRegenUpAmount = 0.1f;

    public void UpgradeEnergyCapacity()
    {
        if (MoneyManager.Spend(30))
        {
            em.UpgradeMaxRegen(energyCapUpAmount);
        }
    }

    public void UpgradeEnergyRegen()
    {
        if (MoneyManager.Spend(30))
        {
            em.energyRegenRate += energyRegenUpAmount;
        }
    }

    public void Build(string tower)
    {
        foreach (GameObject item in GameObject.FindGameObjectsWithTag("Placeholder"))
        {
            Destroy(item);
        }

        GameObject toBuild;
        switch (tower.ToLower())
        {
            case "artillery":
                toBuild = artilleryTowerPlaceholder;
                break;
            case "machinegun":
                toBuild = machineGunTowerPlaceholder;
                break;
            case "shotgun":
                toBuild = shotgunTowerPlaceholder;
                break;
            default:
                throw new Exception("YOUR TOWER BTN STRINGS ARE WRONG");
        }

        Instantiate(toBuild, (Vector2) Camera.main.ScreenToWorldPoint(Input.mousePosition), toBuild.transform.rotation);
    }
}