﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{

	[SerializeField] private GameObject[] sprites;
	private GameObject paths;
	
	
	private SpriteRenderer enemyRenderer;

	private Vector3[] ownPath;
	private float progress = 0;

	// Use this for initialization
	void Start ()
	{
		paths = GameObject.Find("Paths");
		Path script = paths.GetComponent<Path>();

		int rand = Random.Range(0, script.RealPaths.Length);

		ownPath = new Vector3[script.RealPaths[rand].Length];
		ownPath = script.RealPaths[rand];

		transform.position = ownPath[Mathf.FloorToInt(progress)];
		
		Instantiate(sprites[0],transform).transform.SetParent(transform);
		enemyRenderer = GetComponentInChildren<SpriteRenderer>();
		var colval = Random.Range(0f,1f);
		enemyRenderer.color = new Color(1f,Mathf.Max(-colval*2 + 1,0),Mathf.Max((colval*2 - 1) / 2,0));
		var randval = Random.Range(0.1f, 0.2f);
		enemyRenderer.transform.localScale = new Vector3(randval,randval,1f);
	}
	
	// Update is called once per frame
	void Update ()
	{
		if (progress + 1 < ownPath.Length)
		{
			transform.position = Vector3.Lerp(ownPath[Mathf.FloorToInt(progress)], ownPath[Mathf.FloorToInt(progress + 1)],
				progress - Mathf.Floor(progress));
		}
		else
		{
			MoneyManager.money -= 40;
			GetComponent<HealthManager>().Damage(1000);
		}
		
		

		progress += Time.deltaTime;
	}
}
