﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using Array = UnityScript.Lang.Array;
using Random = UnityEngine.Random;

public class Path : MonoBehaviour
{
    [SerializeField] private int gridWidth;
    [SerializeField] private int gridHeight;

    [SerializeField] private float gameWidth;
    [SerializeField] private float gameHeight;

    [SerializeField] private int numOfPaths;

    [SerializeField] private GameObject pathPrefab;
    [SerializeField] private GameObject pathTile;

    public Vector3[][] RealPaths;


    // Use this for initialization
    void Start()
    {
        // move to bottom left
        transform.position = new Vector3(-gameWidth / 2,-gameHeight / 2, 0f);
        
        generatePath();
    }

    private void generatePath()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
        
        

        List<Vector2Int>[] paths = new List<Vector2Int>[numOfPaths];

        // local variables
        bool[][] unusedGrid = new bool[gridWidth][];
        for (int u1 = 0; u1 < unusedGrid.Length; u1++)
        {
            unusedGrid[u1] = new bool[gridHeight];
            for (int u2 = 0; u2 < unusedGrid[u1].Length; u2++)
            {
                unusedGrid[u1][u2] = true;
            }
        }
        Vector2Int gridStart = new Vector2Int(gridWidth / 2, gridHeight / 2);
        unusedGrid[(int) gridStart.x][(int) gridStart.y] = false;
        bool[] pathFinished = Enumerable.Repeat(false, numOfPaths).ToArray();


        for (var pathi = 0; pathi < paths.Length; pathi++)
        {
            paths[pathi] = new List<Vector2Int>();
            paths[pathi].Add(new Vector2Int(gridStart.x, gridStart.y));
        }


        // Create paths
        var maxLoops = 1000;
        while (maxLoops > 0)
        {
            maxLoops--;
            for (var pathNum = 0; pathNum < paths.Length; pathNum++)
            {
                if (pathFinished[pathNum] == true) continue;

                // Find options then add option co-ord for each (R,L,U,D) to options list
                List<Vector2Int> options = new List<Vector2Int>(capacity: 4);

                if (checkGrid(unusedGrid, paths[pathNum][paths[pathNum].Count - 1], 1, 0))
                    options.Add(new Vector2Int(paths[pathNum][paths[pathNum].Count - 1].x + 1, paths[pathNum][paths[pathNum].Count - 1].y));
                if (checkGrid(unusedGrid, paths[pathNum][paths[pathNum].Count - 1], -1, 0))
                    options.Add(new Vector2Int(paths[pathNum][paths[pathNum].Count - 1].x - 1, paths[pathNum][paths[pathNum].Count - 1].y));
                if (checkGrid(unusedGrid, paths[pathNum][paths[pathNum].Count - 1], 0, 1))
                    options.Add(new Vector2Int(paths[pathNum][paths[pathNum].Count - 1].x, paths[pathNum][paths[pathNum].Count - 1].y + 1));
                if (checkGrid(unusedGrid, paths[pathNum][paths[pathNum].Count - 1], 0, -1))
                    options.Add(new Vector2Int(paths[pathNum][paths[pathNum].Count - 1].x, paths[pathNum][paths[pathNum].Count - 1].y - 1));


                // If no options TODO: Backtrace if not at edge
                if (options.Count == 0)
                {
                    pathFinished[pathNum] = true;
                    continue;
                }

                // Select option
                var randomOption = Random.Range(0, options.Count);

                // If at edge
                if (options[randomOption].x == 0 || options[randomOption].x == gridWidth ||
                    options[randomOption].y == 0 || options[randomOption].y == gridHeight)
                {
                    pathFinished[pathNum] = true;
                }

                paths[pathNum].Add(options[randomOption]);
                unusedGrid[options[randomOption].x][options[randomOption].y] = false;
            }

            // Check if finished
            bool allDone = true;
            foreach (bool finished in pathFinished)
            {
                if (finished == false)
                    allDone = false;
            }

            if (allDone) break;
        }

        //TODO: Reverse path order

        // Draw paths
        for (int path = 0; path < numOfPaths; path++)
        {
            GameObject currentPath = Instantiate(pathPrefab, transform);
            currentPath.transform.SetParent(transform);
            for (var vecPoint = 1; vecPoint < paths[path].Count; vecPoint++) // Skip point at tower
            {
                Vector2 averageVec = (paths[path][vecPoint - 1] + paths[path][vecPoint]);
                averageVec.x = averageVec.x / 2 * gameWidth / gridWidth;
                averageVec.y = averageVec.y / 2 * gameHeight / gridHeight;
                GameObject newFiller = Instantiate(pathTile, currentPath.transform);
                newFiller.transform.SetParent(currentPath.transform);
                newFiller.transform.Translate(new Vector3(averageVec.x, averageVec.y, 15f));
                newFiller.name = "Filler";

                GameObject newTile = Instantiate(pathTile, currentPath.transform);
                newTile.transform.SetParent(currentPath.transform);
                newTile.transform.Translate(scaleIntVec2ToVec3(paths[path][vecPoint], gameWidth / gridWidth, gameHeight / gridHeight));
                newTile.name = "x:" + paths[path][vecPoint].x + " y:" + paths[path][vecPoint].y;
            }
        }
        
        // reverse arrays and set to real co-ords
        RealPaths = new Vector3[numOfPaths][];
        for (int path = 0; path < numOfPaths; path++)
        {
            Vector2Int[] flipped = paths[path].ToArray().Reverse().ToArray();

            RealPaths[path] = new Vector3[flipped.Length];
            
            for (int vecPoint = 0; vecPoint < flipped.Length; vecPoint++)
            {
                RealPaths[path][vecPoint] = scaleIntVec2ToVec3(flipped[vecPoint], gameWidth / gridWidth, gameHeight / gridHeight);
                RealPaths[path][vecPoint].x = RealPaths[path][vecPoint].x - gameWidth/2;
                RealPaths[path][vecPoint].y = RealPaths[path][vecPoint].y - gameHeight/2;
            }
        }
    }

    // Check if val + x or y dist is in grid
    private static bool checkGrid(bool[][] TFArray, Vector2Int val, int xdist, int ydist)
    {
        try
        {
            return TFArray[val.x + xdist][val.y + ydist];
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            return false;
        }
    }

    private static Vector3 scaleIntVec2ToVec3(Vector2Int vec2, float scalerx, float scalery)
    {
        return new Vector3(vec2.x * scalerx, vec2.y * scalery, 15f);
    }
}