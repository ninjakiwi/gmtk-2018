﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaceholderScript : MonoBehaviour
{
    [SerializeField] GameObject actualTurret;

    [SerializeField] float price;
    // Update is called once per frame
    void Update()
    {
        if (!BuildingManager.paused)
        {
            Destroy(gameObject);
            return;
        }

        transform.position = (Vector2) Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (Input.GetButtonDown("Fire1") && transform.position.x < 6.1 &&MoneyManager.Spend(price))
        {
            Instantiate(actualTurret, transform.position, Quaternion.identity);
        }
        else if (Input.GetButtonDown("Fire2"))
        {
            Destroy(gameObject);
            return;
        }
    }
}