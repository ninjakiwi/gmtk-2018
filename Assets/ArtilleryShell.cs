﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security;
using EZCameraShake;
using UnityEngine;

public class ArtilleryShell : MonoBehaviour
{
    public Vector2 positionToLand;
    Vector2 startingPos;

    float currentPlace = 0;

    [SerializeField] float speed = 1f;
    [SerializeField] GameObject explosion;
    [SerializeField] GameObject groundedAimer;

    [SerializeField] GameObject areaId;

    [SerializeField] float damage = 0.5f;
    [SerializeField] float damageFalloff = 0.05f;

    GameObject groundInstance;
    SpriteRenderer groundInstanceRenderer;

    SpriteRenderer sr;

    GameObject childSprite;

    // Use this for initialization
    void Start()
    {
        childSprite = transform.Find("ShellSprite").gameObject;
        startingPos = transform.position;
        groundInstance = Instantiate(groundedAimer, positionToLand, Quaternion.identity);
        groundInstanceRenderer = groundInstance.GetComponent<SpriteRenderer>();
        sr = GetComponentInChildren<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        currentPlace += Time.deltaTime * speed;

        groundInstance.transform.localScale = Vector3.Lerp(new Vector3(0.3f, 0.3f), new Vector3(0.2f, 0.2f), currentPlace * 4);
        if (currentPlace < 0.25)
        {
            groundInstance.transform.Rotate(0, 0, Time.deltaTime * speed * 180);
        }

        if (currentPlace < 0.5)
        {
            groundInstanceRenderer.color = Color.Lerp(Color.white, Color.red, currentPlace * 2);
        }
        else
        {
            groundInstanceRenderer.color = Color.Lerp(Color.red, Color.black, (currentPlace - 0.5f) * 2);
        }

        transform.position = Vector3.Lerp(startingPos, positionToLand, currentPlace);

        childSprite.transform.localScale = (Vector3.one * (-Mathf.Pow(2 * currentPlace - 1, 2) + 1) + Vector3.one * 0.2f) * 0.3f;
        sr.color = Color.Lerp(Color.black, Color.white, -Mathf.Pow(2 * currentPlace - 1, 2) + 1);

        if (currentPlace >= 1)
        {
            GetComponent<CircleCollider2D>().enabled = true;
            Collider2D[] stuff = new Collider2D[100];
            int total = GetComponent<CircleCollider2D>().OverlapCollider(new ContactFilter2D(), stuff);
            Instantiate(explosion, transform.position, Quaternion.identity);
            Instantiate(areaId, transform.position, Quaternion.identity);
            GameObject player = GameObject.FindGameObjectWithTag("Player");
            if (player)
            {
                float mag = (transform.position - GameObject.FindGameObjectWithTag("Player").transform.position).magnitude;
                CameraShaker.Instance.ShakeOnce(Mathf.Max(15 - mag, 5), 10, 0.05f, Mathf.Max(0.6f - mag / 20, 0.3f));
            }

            for (int i = 0; i < total; i++)
            {
                if (stuff[i].CompareTag("Enemy"))
                {
                    var hm = stuff[i].GetComponent<HealthManager>();
                    if (!hm)
                    {
                        hm = stuff[i].GetComponentInParent<HealthManager>();
                    }

                    if (hm)
                    {
                        hm.Damage(damage - damageFalloff * (stuff[i].transform.position - transform.position).sqrMagnitude);
                    }
                }
            }

            Destroy(groundInstance);
            Destroy(gameObject);
        }
    }
}