﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorFade : MonoBehaviour
{

	SpriteRenderer sr;

	[SerializeField] float fadeSpeed = 2;
	float pos = 0;

	// Use this for initialization
	void Start ()
	{
		sr = GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update ()
	{
		pos += Time.deltaTime * fadeSpeed;
		if (pos > 1)
		{
			Destroy(gameObject);
			return;
		}
		sr.color = Color.Lerp(new Color(1, 0, 0, 0.3f), new Color(1, 0, 0, 0), pos);


	}
}
