﻿using System.Collections;
using System.Collections.Generic;
using EZCameraShake;
using UnityEngine;

public class Artillery : WeaponTriggerManager
{
    [SerializeField] float rangeDiameter = 10;
    [SerializeField] GameObject artileryShell;
    Transform indicator;

    [SerializeField] GameObject indicatorCrosshair;

    //[SerializeField] GameObject indicatorCrosshairGrounded;

    // Use this for initialization
    new void Start()
    {
        base.Start();
        indicator = transform.Find("ArtilleryRangeIndicator");
        indicator.localScale = Vector3.one * rangeDiameter * 2f;
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
        Vector2 camPos =  Camera.main.ScreenToWorldPoint(Input.mousePosition);
        if (((Vector3)camPos - transform.position).sqrMagnitude <= rangeDiameter * rangeDiameter)
        {
            indicatorCrosshair.transform.position = camPos;
        }
        else
        {
            indicatorCrosshair.transform.position = transform.position + ((Vector3) camPos - transform.position).normalized * rangeDiameter;
        }
    }

    protected override void Fire(Vector3 camPos)
    {
        CameraShaker.Instance.ShakeOnce(2, 5, 0.02f, 0.05f);
        ArtilleryShell art = Instantiate(artileryShell, transform.position, Quaternion.identity).GetComponent<ArtilleryShell>();
        art.positionToLand = indicatorCrosshair.transform.position;
    }
}