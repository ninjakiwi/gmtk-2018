﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthManager : MonoBehaviour
{
    [SerializeField] GameObject deathParticle;
    [SerializeField] float health = 1;
    [SerializeField] float maxHealth = 1;

    [SerializeField] bool fadeIfNotDmged = false;
    [SerializeField] float fadeTime = 1;
    [SerializeField] float actualFadeTime;

    LineRenderer lr;


//	// Use this for initialization
    void Start()
    {
        lr = GetComponent<LineRenderer>();
        UpdateHealth();
    }
//
//	// Update is called once per frame
	void Update () {
	    if (!fadeIfNotDmged) return;

	    actualFadeTime -= Time.deltaTime;
	    if (actualFadeTime <= 0)
	    {
	        lr.enabled = false;
	    }
	    else
	    {
	        lr.enabled = true;
	    }
	}

    public float Health
    {
        get { return health; }
    }


    public void UpgradeMaxHealth(float newMaxHealth)
    {
        maxHealth = newMaxHealth;
        Repair(10000);
    }

    public void Repair(float healthDelta)
    {
        health = Mathf.Min(maxHealth, healthDelta + health);
        UpdateHealth();
    }
    public void Damage(float healthDelta)
    {
        Debug.Log(healthDelta);
        health -= Mathf.Max(0, healthDelta);

        if (health <= 0)
        {
            Instantiate(deathParticle, transform.position, Quaternion.identity);
            Destroy(gameObject);
            if (CompareTag("Enemy"))
            {
                MoneyManager.money += 10;
            }
        }
        else
        {
            UpdateHealth();
        }
    }

    void UpdateHealth()
    {
        actualFadeTime = fadeTime;
        lr.SetPosition(0, new Vector3(-health * 0.2f, lr.GetPosition(0).y, 0));
        lr.SetPosition(1, new Vector3(health * 0.2f, lr.GetPosition(0).y, 0));
    }
}