﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnergyManager : MonoBehaviour
{
    [SerializeField] float currentEnergy;
    [SerializeField] public float energyRegenRate = 0.5f;
    [SerializeField] public float maxEnergy = 1;

    [SerializeField] float delayBeforeRegen = 0.33f;

    float actualDelay;

    [SerializeField] Slider energySlider;

    // Use this for initialization
    void Start()
    {
        energySlider.maxValue = maxEnergy;
    }

    // Update is called once per frame
    void Update()
    {
       
        actualDelay -= Time.deltaTime;
        if (actualDelay <= 0)
        {
            currentEnergy = Mathf.Min(maxEnergy, currentEnergy + Time.deltaTime * energyRegenRate);
            energySlider.value = currentEnergy;
        }
    }

    public void UpgradeMaxRegen(float increaseBy)
    {
        maxEnergy += increaseBy;
        energySlider.maxValue = maxEnergy;
    }

    public bool Drain(float drainAmount)
    {
        if (currentEnergy < drainAmount) return false;
        actualDelay = delayBeforeRegen;
        currentEnergy = Mathf.Max(currentEnergy - drainAmount, 0);
        energySlider.value = currentEnergy;
        return true;
    }
}