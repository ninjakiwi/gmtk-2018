﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security;
using UnityEngine;

public abstract class WeaponTriggerManager : MonoBehaviour
{
    protected enum TriggerType
    {
        SemiAuto,
        Automatic
    };

    protected enum FireBtn
    {
        Fire1,
        Fire2,
        Fire3
    };

    [SerializeField] public float fireRate = 0.5f;
    [SerializeField] public float fireCooldown = 0;

    [SerializeField] public float damage = 0.5f;

    [SerializeField] AudioSource shotSfx;

    [SerializeField] FireBtn fireBtn = FireBtn.Fire1;
    [SerializeField] TriggerType triggerType = TriggerType.Automatic;

    [SerializeField] float energyDrain = 0.1f;

    EnergyManager em;

    // Use this for initialization
    protected void Start()
    {
        GameObject manager = GameObject.FindGameObjectWithTag("Manager");
        em = manager.GetComponent<EnergyManager>();
    }

    // Update is called once per frame
    protected void Update()
    {
        if (BuildingManager.paused)
        {
            return;
        }


        fireCooldown -= Time.deltaTime;

        if (fireBtn == FireBtn.Fire3)
        {
            if (fireCooldown <= 0)
            {
                fireCooldown = fireRate;
                Vector3 lowestPos = Vector3.zero;
                float minFlex = 10000000000;
                foreach (var item in GameObject.FindGameObjectsWithTag("Player"))
                {
                    float temp = (item.transform.position - transform.position).sqrMagnitude;
                    if (minFlex > temp)
                    {
                        minFlex = temp;
                        lowestPos = item.transform.position;
                    }
                }

                Fire(lowestPos);
            }
            return;
        }

        bool triggered = false;
        switch (triggerType)
        {
            case TriggerType.SemiAuto:
                triggered = Input.GetButtonDown(fireBtn.ToString());
                break;
            case TriggerType.Automatic:
                triggered = Input.GetAxis(fireBtn.ToString()) > 0;
                break;
        }

        if (fireCooldown <= 0 && triggered && em.Drain(energyDrain))
        {
            fireCooldown = fireRate;
            var mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos.z = 0;
            Fire(mousePos);
        }
    }

    protected abstract void Fire(Vector3 camPos);
}