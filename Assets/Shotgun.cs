﻿using System.Collections;
using System.Collections.Generic;
using EZCameraShake;
using UnityEngine;

public class Shotgun : WeaponTriggerManager
{
    //[SerializeField] GameObject explosiveOrange;
    [SerializeField] GameObject bullet;

    [SerializeField] int numberOfBullets = 5;
    [SerializeField] float spreadDegrees = 20;

    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
    }

    protected override void Fire(Vector3 camPos)
    {
        var rot = Quaternion.LookRotation(Vector3.forward, camPos - transform.position);

        for (int i = 0; i < numberOfBullets; i++)
        {
            Instantiate(bullet, transform.position + (camPos - transform.position).normalized * 0.3f,
                rot * Quaternion.Euler(0, 0, ((i * spreadDegrees) / numberOfBullets) - spreadDegrees / 2));
        }

        //shotSfx.Play();
        CameraShaker.Instance.ShakeOnce(5, 20, 0.1f, 0.2f);
    }
}