﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{
    Rigidbody2D rb;

    [SerializeField] int forwardSpeed;

    // Use this for initialization
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        float forward = Input.GetAxis("Vertical");
        float right = Input.GetAxis("Horizontal");

        rb.AddForce(((Vector2.up * forward) + (Vector2.right * right)).normalized * forwardSpeed * Time.deltaTime);
        //rb.velocity = ((Vector2.up * forward) + (Vector2.right * right)).normalized * forwardSpeed;
    }
}