﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletShooter : WeaponTriggerManager
{
    [SerializeField] GameObject bulletPrefab;

    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
    }

    protected override void Fire(Vector3 camPos)
    {
        var rot = Quaternion.LookRotation(Vector3.forward, camPos - transform.position);
        Instantiate(bulletPrefab, transform.position + (camPos - transform.position).normalized * 0.3f, rot);
    }
}